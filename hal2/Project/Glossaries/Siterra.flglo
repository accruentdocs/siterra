﻿<?xml version="1.0" encoding="utf-8"?>
<CatapultGlossary>
  <GlossaryEntry>
    <Terms>
      <Term>Allocation</Term>
    </Terms>
    <Definition
      Link="">Functionality that allows users to divide a Payment among different business areas or accounts.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Second payment date</Term>
    </Terms>
    <Definition
      Link="">The date of the second payment (for use in cases such as prorated rent).</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Recurring Payment</Term>
    </Terms>
    <Definition
      Link="">A payment that occurs on a regular basis, such as rent.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Payment Lines</Term>
    </Terms>
    <Definition
      Link="">Each individual amount owed for a period on a Payment.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Payment Header</Term>
    </Terms>
    <Definition
      Link="">Also known as a Payment Card, refers to the upper portion of the Lease home page that contains basic information about the Payment.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Payment</Term>
    </Terms>
    <Definition
      Link="">Functionality that allows users to efficiently manage the constant flow of funds associated with a Lease.  A Payment can be classified as payable or receivable, depending on whether the cash is outgoing or incoming. Additionally, Payments can be categorized as one-time or recurring.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>One Time Payment</Term>
    </Terms>
    <Definition
      Link="">A non-recurring payment.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Lease Quick Links</Term>
    </Terms>
    <Definition
      Link="">A set of links which appear on the User home page in Siterra and provide quick access to various Lease financial pages.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Lease</Term>
    </Terms>
    <Definition
      Link="">A legal contract that is tracked as an object in Siterra and can be associated with payment processes, lease data, property, incidents, and financials.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Finance Manager</Term>
    </Terms>
    <Definition
      Link="">A tool used to bulk process Lease Payment Lines.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Analytics</Term>
    </Terms>
    <Definition
      Link="">A section on the Lease home page that displays combined information from Payments associated with the Lease, including setup information such as currency and measurement.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Percentage Rent</Term>
    </Terms>
    <Definition
      Link="">A type of Payment, reflecting an agreement between a landlord and tenant, in which the tenant pays additional rent based on a percentage of sales.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Payables</Term>
    </Terms>
    <Definition
      Link="">Outgoing Payments associated with a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Receivables</Term>
    </Terms>
    <Definition
      Link="">Incoming Payments associated with a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Exceptions</Term>
    </Terms>
    <Definition
      Link="">A section of the Finance Manager that lists Payments requiring additional attention. Payments are listed in Exceptions if the previous amount is different from the planned amount.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Invoices</Term>
    </Terms>
    <Definition
      Link="">A page within the Finance Manager tool which displays the amount owed for a specific Payment.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Reconciliation</Term>
    </Terms>
    <Definition
      Link="">Refers to the reconciliation of the actual owed amounts with actual paid Payments associated with a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Version</Term>
    </Terms>
    <Definition
      Link="">A stored instance of a Lease used for reference.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Offset</Term>
    </Terms>
    <Definition
      Link="">An amount that is either added or removed from a Payment (e.g., rent abatement).</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Legal Parties</Term>
    </Terms>
    <Definition
      Link="">A section on the Lease home page which stores information regarding litigation contacts and their roles for the Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Clause</Term>
    </Terms>
    <Definition
      Link="">An informational section on the Lease home page that allows users to track key clauses in a contract.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Insurance</Term>
    </Terms>
    <Definition
      Link="">A section on the Lease home page which stores information regarding insurance related to the property.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Holding Interest</Term>
    </Terms>
    <Definition
      Link="">Field which describes whether the user represents the Lessor, Lessee, or another party associated with a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Lessee</Term>
    </Terms>
    <Definition
      Link="">A party that is renting a space.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Lessor</Term>
    </Terms>
    <Definition
      Link="">A party that is leasing a space to a Lessee.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Owned</Term>
    </Terms>
    <Definition
      Link="">A Lease Holding Interest type which refers to property that is owned.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Master Lease</Term>
    </Terms>
    <Definition
      Link="">In reference to a Parent Lease/Sublease relationship, the Master Lease is the primary Lease associated with a Site. Subleases or addendums may be associated with a Master Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Headcount</Term>
    </Terms>
    <Definition
      Link="">The total number of occupants utilizing a space associated with a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Lease Currency</Term>
    </Terms>
    <Definition
      Link="">The monetary denomination associated with the Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Usage</Term>
    </Terms>
    <Definition
      Link="">Field which defines how a space will be utilized (e.g., storage, office, etc.).</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Gross Area</Term>
    </Terms>
    <Definition
      Link="">The total area of the property associated with a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Usable Area</Term>
    </Terms>
    <Definition
      Link="">The amount of space on a property fit for use.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Rentable Area</Term>
    </Terms>
    <Definition
      Link="">The portion of a property that is available for rent.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Pro Rata Share</Term>
    </Terms>
    <Definition
      Link="">Percentage of rentable area over total usable area on a property. Pro Rata share is used in calculating collaborative payments in situations where there are multiple tenants on a property.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Building Rentable Area</Term>
    </Terms>
    <Definition
      Link="">The total amount of rentable area within the building associated with a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Original Commencement Date</Term>
    </Terms>
    <Definition
      Link="">The start date of the original Lease term prior to any renewals or changes.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Base Year</Term>
    </Terms>
    <Definition
      Link="">The year of a Lease term which is used to compare with subsequent years, usually when calculating operating expenses.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Lease Year Date</Term>
    </Terms>
    <Definition
      Link="">The fiscal year date of a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Current Commencement Date</Term>
    </Terms>
    <Definition
      Link="">The start date of the current active Lease term.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Current Expiration Date</Term>
    </Terms>
    <Definition
      Link="">The end date of the current active Lease term.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Original Expiration Date</Term>
    </Terms>
    <Definition
      Link="">The end date of the original Lease term prior to any renewals or changes.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Rent Start Date</Term>
    </Terms>
    <Definition
      Link="">The date on which a tenant begins paying rent.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Move In Date</Term>
    </Terms>
    <Definition
      Link="">The date on which a tenant is supposed to begin occupying a property.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Move Out Date</Term>
    </Terms>
    <Definition
      Link="">The date on which a tenant is supposed to vacate a property.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Execution Date</Term>
    </Terms>
    <Definition
      Link="">The date a Lease is executed. Execution Date usually refers to the date a Lease is signed by both parties.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Lease Measurement</Term>
    </Terms>
    <Definition
      Link="">The unit of measurement associated with the Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Expense Reconciliation</Term>
    </Terms>
    <Definition
      Link="">A paid additional module which helps users manage operating expense obligations such as common area maintenance, insurance, and tax reconciliation. This functionality compares actual Payments with amounts owed, allowing users to request reimbursement for overpaid Payment Lines or submit payment for underpaid Payment Lines.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>TI Allowance</Term>
    </Terms>
    <Definition
      Link="">Funds a landlord sets aside for the tenant to improve a rented property, usually given as a rent discount. In Siterra, the TI Allowance module allows users to manage these funds.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Stream</Term>
    </Terms>
    <Definition
      Link="">An aggregated list of Payment Lines across all Leases on a Site.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Abstract Report</Term>
    </Terms>
    <Definition
      Link="">A document that summarizes a Lease in its entirety, including Payments, Allocations, Escalations, and more.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Abstract Validation</Term>
    </Terms>
    <Definition
      Link="">An alternate, simplified version of the Abstract Report.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Parent Lease</Term>
    </Terms>
    <Definition
      Link="">Refers to the relationship between a Lease and its Sub-lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Critical Dates/Options</Term>
    </Terms>
    <Definition
      Link="">A section on the Lease home page which defines key dates and information associated with a Lease, including renewal dates and termination dates. Lease Critical Dates/Options appear in To-Dos for assigned users.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Release (Payments)</Term>
    </Terms>
    <Definition
      Link="">An action taken on a Payment which removes it from a Hold status.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Reject (Payments)</Term>
    </Terms>
    <Definition
      Link="">An action taken on a Payment which resets an Exported status back to Planned.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Pay (Payments)</Term>
    </Terms>
    <Definition
      Link="">Indicates that a Payment has been saved and the recipient has received physical payment. A paid Payment is no longer editable.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Void (Payments)</Term>
    </Terms>
    <Definition
      Link="">A Payment status indicating that a check or other physical payment has been voided. Voiding Payments is not reversable.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Hold (Payments)</Term>
    </Terms>
    <Definition
      Link="">Stopping a Payment from being processed in the Finance Manager and indicating that an action needs to be taken on the Payment.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Cancel (Payments)</Term>
    </Terms>
    <Definition
      Link="">Stopping a Payment from being processed in the Finance Manager and indicating that physical payment (e.g., a check) should not be sent.

Canceled Payments cannot be restored.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Approve (Payments)</Term>
    </Terms>
    <Definition
      Link="">A status that indicates validation of a Payment Line and allows Payments to be exported through the Finance Manager in order to be paid.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Breakpoint (SLR)</Term>
    </Terms>
    <Definition
      Link="">SLR Breakpoints are used to track changes made to Straight Line Rent (SLR). They are triggered whenever certain actions are taken on Leases or Payments, causing SLR to recalculate.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Prime Lease</Term>
    </Terms>
    <Definition
      Link="">Field on the Lease home page which denotes the Lease as being the primary or master Lease associated with a Site.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Breakpoint (Percent Rent)</Term>
    </Terms>
    <Definition
      Link="">Functionality which allows users to set natural or unnatural breakpoints for automatic calculation of percentage rent obligation based on gross sales entry.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Breakpoint Tier</Term>
    </Terms>
    <Definition
      Link="">A section on a percentage rent Payment that allows for storage of Breakpoint information.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Effective Date</Term>
    </Terms>
    <Definition
      Link="">The date on which a Critical Date/Option goes into effect.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Archive</Term>
    </Terms>
    <Definition
      Link="">The process of removing an object from Siterra without deleting it. Archiving relocates the object to Archive Manager. An archived object is able to be restored, but archived objects cannot be reported on or accessed.

When an object is archived, its child objects are archived as well.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Straight Line Rent (SLR)</Term>
    </Terms>
    <Definition
      Link="">A paid additional module that smooths Payments over a Lease term, providing a more accurate reflection of rent obligations over the lifetime of a Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Unit</Term>
    </Terms>
    <Definition
      Link="">A sub-folder within a portfolio which allows users to categorize and manage sections of the portfolio.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Search Ring</Term>
    </Terms>
    <Definition
      Link="">A cluster or grouping of Sites which can be associated with Projects, Libraries, Photo Libraries, and more.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Site</Term>
    </Terms>
    <Definition
      Link="">A location or property that is tracked as an object in the system. Sites allow users to manage and integrate data, documents, photos, other information associated with a physical property.</Definition>
  </GlossaryEntry>
  <GlossaryEntry
    TermClass="Expanding">
    <Terms>
      <Term>Asset</Term>
    </Terms>
    <Definition
      Link="">Equipment or material that is tracked as an object in the system.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Component</Term>
    </Terms>
    <Definition
      Link="">A type of Asset that is a child object of another Asset.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Incident</Term>
    </Terms>
    <Definition
      Link="">An unplanned event that takes place on a Site, Asset, or Lease.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Event</Term>
    </Terms>
    <Definition
      Link="">A planned Event that occurs on a Site.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Document Library</Term>
    </Terms>
    <Definition
      Link="">A collection of files containing documents associated with an object. Libraries can be associated with Units, Search Rings, Sites, Assets, or Leases.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Photo Library</Term>
    </Terms>
    <Definition
      Link="">A collection of files containing photos associated with an object. Libraries can be associated with Units, Search Rings, Sites, Projects, Assets, or Leases.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Schedule Library</Term>
    </Terms>
    <Definition
      Link="">A collection of files associated with a Project.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>General Library</Term>
    </Terms>
    <Definition
      Link="">A collection of files associated with the entire portfolio.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Project</Term>
    </Terms>
    <Definition
      Link="">A workflow schedule that is tracked as an object in the system and can be associated with a Site, Search Ring, Asset, Vendor, or Event.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Task</Term>
    </Terms>
    <Definition
      Link="">An action associated with a Project that is tracked in the system. Tasks are made up of one or more Subtasks.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Subtask</Term>
      <Term>Deliverable</Term>
    </Terms>
    <Definition
      Link="">An action associated with a Task that is tracked in the system. The terms "Subtask" and "deliverable" are used interchangeably in Siterra.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Ad-hoc Task</Term>
    </Terms>
    <Definition
      Link="">A Task created as needed on a Project Schedule that is not associated with a Schedule Template.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Approval Subtask</Term>
    </Terms>
    <Definition
      Link="">A Project deliverable associated with a Task which requires a certain user's approval to be marked complete.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Document Subtask</Term>
    </Terms>
    <Definition
      Link="">A Project deliverable associated with a Task which requires a user to upload a document in order to be marked complete.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Form Subtask</Term>
    </Terms>
    <Definition
      Link="">A Project deliverable associated with a Task which requires a user to complete a form in order to be marked complete.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Section</Term>
    </Terms>
    <Definition
      Link="">A header, usually containing a grid, which can be pinned open or closed.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Quick Navigation Bar</Term>
    </Terms>
    <Definition
      Link="">An expanding panel that appears on the left side of the browser throughout Siterra which allows for quick navigation throughout the application.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Navigation Hierarchy</Term>
    </Terms>
    <Definition
      Link="">The hierarchical structure of organizational units in Siterra which allows users to navigate the application.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Hub</Term>
    </Terms>
    <Definition
      Link="">A collection of Antennas.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Object Creator Subtask</Term>
    </Terms>
    <Definition
      Link="">A type of Subtask associated with Advanced Projects which forces a user to create or update a child object in order to be marked complete</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Vendor</Term>
    </Terms>
    <Definition
      Link="">A company associated with the portfolio which can be associated with various objects throughout the system.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Clipboard</Term>
    </Terms>
    <Definition
      Link="">A tool which allows users to store commonly used documents in order to add, upload, and move them to multiple locations.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Project Tracker</Term>
    </Terms>
    <Definition
      Link="">A tool that allows users with certain permissions to manage multiple Project dates at once.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Archive Manager</Term>
    </Terms>
    <Definition
      Link="">An area that contains all objects that have been removed from Siterra using the archive function.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Antenna</Term>
    </Terms>
    <Definition
      Link="">An object which represents an individual small cell component, such as a femtocell, picocell, or DAS node.</Definition>
  </GlossaryEntry>
  <GlossaryEntry>
    <Terms>
      <Term>Library</Term>
    </Terms>
    <Definition
      Link="">A collection of files containing documents or photos associated with an object</Definition>
  </GlossaryEntry>
</CatapultGlossary>